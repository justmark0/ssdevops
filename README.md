# Lab 1 - Team 2
Mark Nicholson
Kseniya Kudasheva
Andrey Khoroshavin

## Task 1

1. A pipeline in software development is a =method that drives software development through a path of building, testing, and deploying code, also known as CI/CD (Continuous Integration and Continuous Deployment)
2. A CI/CD pipeline automates your software delivery process. The pipeline builds code, runs tests (CI), and safely deploys a new version of the application (CD). Automated pipelines remove manual errors, provide standardized feedback loops to developers, and enable fast product iterations.
3. Correct order:
3.1. Requirement analysis answers for question “What problems need solutions?”
3.2. Planning answers the question "What do we want to do?"
3.3. Architectural design answers the question "How will we achieve our goals?"
3.4. Build
Software development governs the process of creating a product. 
5. Test
Testing determines the quality of the product. 
6. Deploy
Deployment governs the use of the final product



## Task 2
For deployment to be available for all teammates (almost) at any time, we deployed it on Yandex Cloud virtual machine.  
Also, we decided to deploy GitLab and GitLab runner on the same machine (due to Bonus 1) and Deployment server on another host.  
  
To deploy GitLab instance we:
1. Installed Docker on Ubuntu host - `sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin` (see details [here](https://docs.docker.com/engine/install/ubuntu/))

2. Added GitLab docker image and ran container for it. We used the code provided in task but changed lines 
    ```
    --hostname team2.devsecops \
    --publish 443:443 --publish 80:80 --publish 234:22 \
    ```
> Port 22 on machine is already allocated by ssh

3. After some time access the web-service via `http://team2.devsecops/`  
[!alt text](pic1.png)

4. Enter to GitLab container terminal `sudo docker exec -it gitlab /bin/bash` and use `gitlab-rake "gitlab:password:reset"` to set new password for root user

5. On page `/admin/users` add new users for each of us
[!alt text](pic2.png)


## Task 3
Dockerize the application and deploy it to the server.
Dockerfile:
```
FROM python:3.10.7-buster

ENV PYTHONDONTWRITEBYTECODE=1
ENV FLASK_APP=app.py

RUN useradd -ms /bin/bash app
WORKDIR /home/app

RUN pip install --upgrade pip poetry==1.1.11 && poetry config virtualenvs.create false
COPY . .
RUN poetry install --no-dev

USER app

CMD ["flask", "run",  "--host", "0.0.0.0", "--port", "8000"]
```

docker-compose is simple:
```
version: '3'

services:
  app:
    build: .
    restart: always
    ports:
      - "8000:8000"
```

## Task 4
1. `sudo useradd team2` and create ssh key via `ssh-keygen -t rsa -C "ssdevops"`
2. add public key to gitlab. We've named it as USER_PUBKEY
3. gitlab-ci.yml (using local registry):
```
stages:
  - build
  - deploy

build:
  stage: build
  script:
    - docker build -t python -f deploy/python/Dockerfile .
    - docker tag python python:latest
    - echo 'Pub key is $USER_PUBKEY it is useless, but assighment were required to do so...'


run_app:
  stage: deploy
  script:
    - docker-compose -f docker-compose.yml -p timeApp stop
    - docker-compose -f docker-compose.yml -p timeApp rm -f
    - docker-compose -f docker-compose.yml -p timeApp up -d

```

[!alt text](pic4.png)

[!alt text](pic5.png)

